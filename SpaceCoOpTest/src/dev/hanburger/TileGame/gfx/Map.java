package dev.hanburger.TileGame.gfx;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import dev.hanburger.TileGame.Game;

public class Map {
	
	private int width,height;
	
	private ArrayList<BufferedImage> imageData;
	private ArrayList<Integer> data;
	private ArrayList<Animation> animationSprites;
	private int animationCounter=0;
	
	private Game game;

	private BufferedImage map;
	
	public Map(Game game, int width,int height, ArrayList<Integer> data)
	{
		this.width=width;
		this.height=height;
		this.game=game;
		if(game==null)
		{
			System.out.println("I mean wut");
		}
		imageData = new ArrayList<>();
		this.data=data;
		
		init();
		
		
		
	}

	private void init() {
		// TODO Auto-generated method stub
		animationSprites = new ArrayList<>();
		
		for(Integer e : data)
		{
			imageData.add(Assets.mapSprites.get(e));
			
			
		}
		int counter =0;
		for(int y = 0 ; y < height ; y++)
		{
			for(int x = 0; x < width; x++)
			{
				if(Assets.animationIDS.contains(data.get(counter)))
				{
					animationSprites.add(new Animation(Assets.returnAnimationSprites(data.get(counter)),
							40*x - game.getGameCamera().getxOffset(),40*y - game.getGameCamera().getyOffset()));
				}
				
				if(game.getColDet().getCollisionIDS().contains(data.get(counter)))
				{
					game.getColDet().getBlocks().add(new Rectangle2D.Float(40*x, 40*y, 40, 40));
				}
				
				counter++;
			}
		}
		
		map = new BufferedImage(width*40,height*40,6);
		
		Graphics2D loader = map.createGraphics();
		
		int counter2 =0;
		for(int y = 0; y < height; y++)
		{
			for(int x = 0 ; x < width; x++)
			{
				loader.drawImage(imageData.get(counter2),(int)((40*x)-game.getGameCamera().getxOffset()),
						(int)((40*y)-game.getGameCamera().getyOffset()),40,40,null);
				counter2++;
				
				
			}
		}
		
		
		
		
		
		
	}
	
	
	
	
	public void tick()
	{
		
		
		if(animationCounter < 35)
		{
			animationCounter++;
		}
		else
		{
			animationCounter=0;
			for(Animation e : animationSprites)
			{
				
				e.update();
			}
		}
		
		for(Animation e : animationSprites)
		{
			e.setCameraOffset(game.getGameCamera().getxOffset(),
					game.getGameCamera().getyOffset());
		}
	}
	
	public void render(Graphics g)
	{
		
		//System.out.println(width + ", " + height);
//		int counter =0;
//			for(int y = 0; y < height; y++)
//			{
//				for(int x = 0 ; x < width; x++)
//				{
//					g.drawImage(imageData.get(counter),(int)((32*x)-game.getGameCamera().getxOffset()),
//							(int)((32*y)-game.getGameCamera().getyOffset()),32,32,null);
//					counter++;
//					
//					
//				}
//			}
		
		g.drawImage(map,(int)( 0-game.getGameCamera().getxOffset()),(int)( 0-game.getGameCamera().getyOffset()), null);
			
			
		for(Animation e : animationSprites)
		{
			
			e.render(g);
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public ArrayList<BufferedImage> getImageData() {
		return imageData;
	}
	
	
	
	

}
