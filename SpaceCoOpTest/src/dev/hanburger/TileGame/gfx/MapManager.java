package dev.hanburger.TileGame.gfx;

import java.awt.Graphics;
import java.io.IOException;
import java.util.ArrayList;

import dev.hanburger.TileGame.Game;

public class MapManager {
	
	private ArrayList<Map> maps;
	
	private int currentMap;
	
	private Game game;
	
	
	public MapManager(Game game)
	{
		maps = new ArrayList<>();
		this.game=game;
		currentMap=0;
		init();
	}
	
	
	private void init() {
		// TODO Auto-generated method stub
		try {
			maps.add(new MapLoader(game).loadMap("/maps/testmap7.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void tick()
	{
		maps.get(currentMap).tick();
	}
	
	public void render(Graphics g)
	{
		maps.get(currentMap).render(g);
	}

}
