package dev.hanburger.TileGame.gfx;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Assets {
	
	private static final int width = 8, height=8;
	private static SpriteSheet sheet;
	
	public static BufferedImage player;
	public static BufferedImage textBox;
	
	public static ArrayList<BufferedImage> sprites;
	public static ArrayList<BufferedImage> mapSprites;
	
	public static ArrayList<BufferedImage> walkingUp;
	public static ArrayList<BufferedImage> walkingDown;
	public static ArrayList<BufferedImage> walkingLeft;
	public static ArrayList<BufferedImage> walkingRight;
	
	public static ArrayList<BufferedImage> water;
	
	public static ArrayList<Integer> animationIDS;
	
	
	

	
	public static void init()
	{
		sheet = new SpriteSheet(ImageLoader.loadImage("/textures/sprite_sheet8x8UV35.png"));
		
		sprites = new ArrayList<>();
		mapSprites = new ArrayList<>();
		
		player = sheet.crop(0, 0, width, height);
		textBox = sheet.crop(0, height*9,4*width , 2 * height);
		//System.out.println(player.getType());
		
		initializeSprites();
		initializeMapSprites();
		
		initializePlayer();
		
		initializeAnimationIdentifiers();
		initializeWaterAnimation();
		
		
	}
	
	
	
	private static void initializeAnimationIdentifiers()
	{
		animationIDS=new ArrayList<>();
		animationIDS.add(161);
		
	}
	
	private static void initializePlayer()
	{
		walkingDown = new ArrayList<>();
		walkingDown.add(sprites.get(295));
		walkingDown.add(sprites.get(295 +32));
		walkingDown.add(sprites.get(295 +64));
		walkingDown.add(sprites.get(295 +96));
		
		walkingUp = new ArrayList<>();
		walkingUp.add(sprites.get(298));
		walkingUp.add(sprites.get(298 +32));
		walkingUp.add(sprites.get(298 +64));
		walkingUp.add(sprites.get(298 +96));
		
		walkingLeft = new ArrayList<>();
		walkingLeft.add(sprites.get(297));
		walkingLeft.add(sprites.get(297 +32));
		walkingLeft.add(sprites.get(297 +64));
		walkingLeft.add(sprites.get(297 +96));
		
		walkingRight = new ArrayList<>();
		walkingRight.add(sprites.get(296));
		walkingRight.add(sprites.get(296 +32));
		walkingRight.add(sprites.get(296 +64));
		walkingRight.add(sprites.get(296 +96));
		
		
	}
	
	private static void initializeWaterAnimation()
	{
		water = new ArrayList<>();
		water.add(sprites.get(160));
		water.add(sprites.get(161));
		water.add(sprites.get(162));
		water.add(sprites.get(163));
		water.add(sprites.get(160));
		water.add(sprites.get(163));
		water.add(sprites.get(162));
		water.add(sprites.get(161));
		//water.add(sprites.get(160));
	}
	
	private static void initializeSprites()
	{
		
		for(int y=0; y < 32; y++)
		{
			for(int x=0; x < 32; x++ )
			{
				sprites.add(sheet.crop(x*width, y *height, width, height));
			}
		}
		
	}
	
	private static void initializeMapSprites()
	{
		mapSprites.add(null);
		
		for(BufferedImage e : sprites)
		{
			mapSprites.add(e);
		}
	}
	
	public static ArrayList<BufferedImage> returnAnimationSprites(int value)
	{
		switch(value)
		{
		case 161:
			return water;
			
		}
		return null;
		
	
	}
	

}
