package dev.hanburger.TileGame.CollisionDetection;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.entities.Entity;
import dev.hanburger.TileGame.entities.creatures.Npc;

public class CollisionDetection {
	
	private ArrayList<Entity> entities;
	private ArrayList<Rectangle2D> blocks;
	private ArrayList<Integer> collisionIDS;
	private Game game;
	
	public CollisionDetection(Game game)
	{
		entities = new ArrayList<>();
		blocks = new ArrayList<>();
		collisionIDS= new ArrayList<>();
		this.game=game;
		init();
	}

	private void init() {
		// TODO Auto-generated method stub
		collisionIDS.add(161);
		collisionIDS.add(162);
		collisionIDS.add(163);
		collisionIDS.add(164);
		
		collisionIDS.add(522);
		collisionIDS.add(523);
		collisionIDS.add(524);
		
		
		collisionIDS.add(554);
		collisionIDS.add(555);
		collisionIDS.add(556);
		
		collisionIDS.add(586);
		collisionIDS.add(587);
		collisionIDS.add(588);
		
		collisionIDS.add(9);
		collisionIDS.add(10);
		collisionIDS.add(11);
		collisionIDS.add(14);
		collisionIDS.add(15);
		collisionIDS.add(15+32);
		collisionIDS.add(16);
		collisionIDS.add(16+32);
		
		collisionIDS.add(12);
		collisionIDS.add(13);
		collisionIDS.add(12+32);
		collisionIDS.add(13+32);
		collisionIDS.add(12+64);
		collisionIDS.add(13+64);
		
		//Green tree
		collisionIDS.add(417);
		collisionIDS.add(418);
		collisionIDS.add(419);
		
		collisionIDS.add(417 +32);
		collisionIDS.add(418 +32);
		collisionIDS.add(419 +32);
		
		collisionIDS.add(417 +64);
		collisionIDS.add(418 +64);
		collisionIDS.add(419 +64);
		
		//House
		
		collisionIDS.add(141);
		collisionIDS.add(142);
		collisionIDS.add(143);
		collisionIDS.add(144);
		
		collisionIDS.add(141+32);
		collisionIDS.add(142+32);
		collisionIDS.add(143+32);
		collisionIDS.add(144+32);
		
		collisionIDS.add(141+64);
		collisionIDS.add(142+64);
		collisionIDS.add(143+64);
		collisionIDS.add(144+64);
		
		collisionIDS.add(141+96);
		//collisionIDS.add(142+96);
		collisionIDS.add(143+96);
		collisionIDS.add(144+96);
		
		
		
		
		
	}
	
	public boolean checkCollision(float x, float y,Entity b)
	{
		for(Rectangle2D e : blocks)
		{
			if(e.intersects(x, y, 40, 40))
			{
				return true;
				
			}
			
		}
		
		for(Entity e : entities)
		{
			if(e.getBox().intersects(x, y, 40, 40) && !e.equals(b))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public Npc returnCollision(float x,float y, Entity b)
	{
		for(Entity e : entities)
		{
			if(e.getBox().intersects(x, y, 40, 40) && !e.equals(b))
			{
				if(e instanceof Npc)
				return (Npc)e;
			}
		}
		
		return null;
	}

	public ArrayList<Integer> getCollisionIDS() {
		return collisionIDS;
	}

	public void setCollisionIDS(ArrayList<Integer> collisionIDS) {
		this.collisionIDS = collisionIDS;
	}

	public ArrayList<Entity> getEntities() {
		return entities;
	}

	public ArrayList<Rectangle2D> getBlocks() {
		return blocks;
	}
	
	
	

}
