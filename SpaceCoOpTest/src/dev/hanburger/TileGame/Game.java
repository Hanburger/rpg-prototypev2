package dev.hanburger.TileGame;

import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import dev.hanburger.TileGame.CollisionDetection.CollisionDetection;
import dev.hanburger.TileGame.display.Display;
import dev.hanburger.TileGame.gfx.Assets;
import dev.hanburger.TileGame.gfx.FontLoader;
import dev.hanburger.TileGame.gfx.GameCamera;
import dev.hanburger.TileGame.gfx.MapManager;
import dev.hanburger.TileGame.input.KeyManager;
import dev.hanburger.TileGame.states.GameState;
import dev.hanburger.TileGame.states.MenuState;
import dev.hanburger.TileGame.states.State;

public class Game implements Runnable {
	
	private Display display;
	
	
	
	public int width,height;
	public String title;
	
	private boolean running=false;
	private Thread thread;
	
	private BufferStrategy bs;
	private Graphics g;
	
	
	private State gameState;
	private State menuState;
	
	private KeyManager keyManager;
	
	private MapManager mapManager;
	
	private GameCamera camera;
	
	private CollisionDetection colDet;
	
	public Game(String title,int width,int height)
	{
		this.width=width;
		this.height=height;
		this.title=title;
		keyManager= new KeyManager();
		
		System.setProperty("sun.java2d.opengl", "True");
		
		
	}
	int x=0;

	@Override
	public void run() {
		// TODO Auto-generated method stub
		init();
		
		int fps = 60;
		double timePerTick = 1000000000 /fps;
		double delta =0;
		long now;
		long lastTime = System.nanoTime();
		long timer = 0;
		int ticks = 0;
		
		
		while(running)
		{
			now = System.nanoTime();
			delta+=(now - lastTime) / timePerTick;
			timer += now - lastTime;
			lastTime = now;
			
			
			
			if(delta >= 1)
			{
				tick();
				render();
				ticks++;
				delta --;
			}
			
			if(timer >= 1000000000)
			{
				System.out.println("Times and Frames: " + ticks);
				ticks=0;
				timer=0;
			}
		}
		
		stop();
	}
	
	private void init() {
		// TODO Auto-generated method stub
		display = new Display(title,width,height);
		display.getFrame().addKeyListener(keyManager);
		Assets.init();
		FontLoader.loadFont();
		camera = new GameCamera(this,0,0);
		colDet = new CollisionDetection(this);
		mapManager= new MapManager(this);
		gameState = new GameState(this);
		menuState = new MenuState(this);
		
		State.setState(gameState);
	}
	
	
	public CollisionDetection getColDet() {
		return colDet;
	}

	public MapManager getMapManager() {
		return mapManager;
	}

	private void tick()
	{
		keyManager.tick();
		if(State.getState()!=null)
		{
			State.getState().tick();
		}
	}
	
	private void render()
	{
		bs = display.getCanvas().getBufferStrategy();
		if(bs == null)
		{
			display.getCanvas().createBufferStrategy(3);
			return;
		}
		
		g = bs.getDrawGraphics();
		
		g.clearRect(0, 0, width, height);
		
		
		//g.drawImage(Assets.player, x, 10,32,32, null);
		if(State.getState()!=null)
		{
			State.getState().render(g);
		}
		
		
		bs.show();
		g.dispose();
	}


	public synchronized void start()
	{
		if(running)
		{
			return;
		}
		
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	public synchronized void stop()
	{
		if(!running)
		{
			return;
		}
		running =false;
		
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public KeyManager getKeyManager()
	{
		return keyManager;
	}
	
	public GameCamera getGameCamera()
	{
		return camera;
	}
	

}
