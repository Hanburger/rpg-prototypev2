package dev.hanburger.TileGame.entities.creatures;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.gfx.Animation;
import dev.hanburger.TileGame.gfx.Assets;

public class Player extends Creature{
	
	
	
	
	
	private String lastUsed="";
	private boolean cx,cy;
	
	
	private int animationIndex;
	private Animation up,down,left,right;
	private ArrayList<Animation> directions;
	private boolean isWalking;
	private int animationCounter=0;
	private boolean locked=false;
	
	
	
	
	


	public Player(Game game, float x, float y) {
		super(game,x, y);
	
		animationIndex=0;
		initAnimations();
		 
		
		 
		
	}
	
	public void initAnimations()
	{
		up = new Animation(Assets.walkingUp, x, y);
		up.setOffset(2);
		down = new Animation(Assets.walkingDown, x, y);
		down.setOffset(2);
		left = new Animation(Assets.walkingLeft, x, y);
		left.setOffset(2);
		right = new Animation(Assets.walkingRight, x, y);
		right.setOffset(2);
		
		directions = new ArrayList<>();
		directions.add(up);
		directions.add(down);
		directions.add(left);
		directions.add(right);
		isWalking=false;
	}

	@Override
	public void tick() {
		
		//CollisionDetection.rectangles.set(0, new Rectangle2D.Float(x,y,32,32)); 
		//CollisionDetection.rectangles.get(0).setRect(x, y, 32, 32);
		box.setRect(x, y, 40, 40);
		
		cx = isInTile(x);
		cy = isInTile(y);
		
		if(!locked)
		{
		update();
		
		
		move();
		
		
		updateCamera();
		}	
		
		//System.out.println(x+ ", " + y);
		
		//isMoving = false;
		
		
		
		}
	
	
	public void lock()
	{
		locked=true;
	}
	
	public void unlock()
	{
		locked = false;
	}
	
	public String getLastUsed() {
		return lastUsed;
	}

	private void updateCamera()
	{
		for(Animation e: directions)
		{
			e.setCameraOffset(game.getGameCamera().getxOffset(), game.getGameCamera().getyOffset());
		}
		
		game.getGameCamera().centerOnEntity(this);
		
		
	}
	
	private void update()
	{
		for(int i = 0 ; i <4 ; i++)
		{
			directions.get(i).updateLoc(x, y);
		}
		
		
		if(isWalking==true)
		{
			if(animationCounter >7)
			{
				animationCounter=0;
				directions.get(animationIndex).update();
			}
			else
			{
				animationCounter++;
			}
		}
		else
		{
			animationCounter=0;
		}
	}
	
	private void move()
	{
		if(cy&& cx)
		{
		if(game.getKeyManager().up)
		{
			animationIndex=0;
			isWalking=true;
			lastUsed = "w";
			if(!game.getColDet().checkCollision(x, y-2,this))
			{
				
				animationIndex=0;
				y -=2;
				//lastUsed = "w";
				
			}
		}
		else if(game.getKeyManager().down )
		{
			animationIndex=1;
			isWalking=true;
			lastUsed = "d";
			if(!game.getColDet().checkCollision(x, y+1,this))
			{
				y += 2;
				animationIndex=1;
				//lastUsed = "d";
			}
		}
		else if(game.getKeyManager().left )
		{
			animationIndex =2;
			isWalking=true;
			lastUsed = "l";
			if(!game.getColDet().checkCollision(x-2, y,this))
			{
				x -= 2;
				animationIndex =2;
				//lastUsed = "l";
			}
		}
		else if(game.getKeyManager().right)
		{
			animationIndex= 3;
			isWalking=true;
			lastUsed = "r";
			if(!game.getColDet().checkCollision(x+2, y,this))
			{
				x += 2;
				animationIndex= 3;
				//lastUsed = "r";
			}
		}
		else
		{
			isWalking=false;
		}
		}
	
		
		
		
		
		
		if(!cy)
		{
			//isMoving = true;
			if(lastUsed.equals("w"))
			{
				//if(!CollisionDetection.checkCollision(x, y-2,0))
				y-=2;
				//else
					//y+=2;
			}
			if(lastUsed.equals("d"))
				//if(!CollisionDetection.checkCollision(x, y+2,0))
					y+=2;
					//else
						//y-=2;
			//return;
			
			
		}
		
		if(!cx)
		{
			//isMoving = true;
			if(lastUsed.equals("l"))
				//if(!CollisionDetection.checkCollision(x-2, y,0))
				x-=2;
				//else
					//x+=2;
			if(lastUsed.equals("r"))
				//if(!CollisionDetection.checkCollision(x+2, y, 0))
				x+=2;
				//else
					//x-=2;
			//return;
			
			
		}
	}
		

	@Override
	public void render(Graphics g) {
		String test = "playerA";
		//Graphics2D g2 = (Graphics2D) g;
		//g.drawImage(Assets.playerAnimation.get(animationIndex) , (int) x, (int) y, 32, 32, null);
		//renderExtra(g);
		
		if(isWalking==true)
		{
			directions.get(animationIndex).render(g);
		}
		else{
			g.drawImage(getIdleImage(animationIndex),
					(int) x-2 - (int) game.getGameCamera().getxOffset(),
					(int) y - (int) game.getGameCamera().getyOffset(),40,40,null);
		}
		
		
	}
	
	private BufferedImage getIdleImage(int index)
	{
		switch(index)
		{
		case 0:	return Assets.walkingUp.get(0);
		case 1: return Assets.walkingDown.get(0);
		case 2: return Assets.walkingLeft.get(0);
		case 3: return Assets.walkingRight.get(0);
		
		}
		return null;
	}
	
	
	private boolean isInTile(float coord)
	{
		if(coord%40 ==0)
		{
			return true;
		}
		return false;
		
	}
	
	public float getXCoords()
	{
		return x;
	}
	public float getYCoords()
	{
		return y;
	}
	
	
	
	
	public void setCoords(float x, float y)
	{
		this.x=x;
		this.y=y;
	}
	

}
