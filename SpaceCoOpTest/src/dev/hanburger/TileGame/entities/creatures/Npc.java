package dev.hanburger.TileGame.entities.creatures;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.gfx.Assets;
import dev.hanburger.TileGame.gfx.TextBlock;


public class Npc extends Creature{
	
	private Game game;
	private float x,y;
	private String lastUsed= "";

	private boolean cx,cy;
	
	private int chance = 0;
	private int animationIndex;
	
	private boolean locked=false;
	
	private Random random;
	
	private ArrayList<TextBlock> messages;
	
	public Npc(Game game, float x, float y)
	{
		super(game,x,y);
		this.x=x;
		this.y=y;
		this.game = game;
		random = new Random();
		this.messages= new ArrayList<>();
		messages.add(new TextBlock("I am the villager","of this town!"));
		messages.add(new TextBlock("My name is Han!",null));
		
		//messages.add("My name is Han!");
		
		this.animationIndex=0;
		
	}

	public ArrayList<TextBlock> getMessages() {
		return messages;
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		
		//CollisionDetection.rectangles.set(colID, new Rectangle2D.Float(x,y,32,32));
		
		box.setRect(x, y, 40, 40);
		
		cx = isInTile(x);
		cy = isInTile(y);
		
		chance = random.nextInt(500);
		//System.out.println(chance);
		
		if(cy && cx)
		{
			if(!locked)
			{
			
			if(chance == 5 )
			{
				animationIndex=2;
				//System.out.println("for");
				if(!game.getColDet().checkCollision(x, y-2,this))
				{
					animationIndex=2;
					y -= 2;
					lastUsed="w";
				}
			}
			if(chance == 3)
			{
				animationIndex=0;
				//System.out.println("back");
				if(!game.getColDet().checkCollision(x, y+2,this))
				{
					animationIndex=0;
					y +=2;
					lastUsed="d";
				}
			}
			if(chance == 7)
			{
				animationIndex=3;
				//System.out.println("left");
				if(!game.getColDet().checkCollision(x-2, y,this))
				{
					animationIndex=3;
					x-=2;
					lastUsed="l";
				}
			}
			if(chance ==9)
			{
				animationIndex=1;
				//System.out.println("right + " + chance);
				if(!game.getColDet().checkCollision(x+2, y,this))
				{
					animationIndex=1;
					//System.out.println("Initiate");
					x+=2;
					lastUsed="r";
				}
			}
			
			}
		}
		
		if(!cy)
		{
			
			if(lastUsed.equals("w"))
			{
				if(!game.getColDet().checkCollision(x, y-2,this))
				y-=2;
				else
					y+=2;
			}
			if(lastUsed.equals("d"))
				if(!game.getColDet().checkCollision(x, y+2,this))
					y+=2;
					else
						y-=2;
		}
		else{
		}
		if(!cx)
		{
			if(lastUsed.equals("l"))
				if(!game.getColDet().checkCollision(x-2, y,this))
				x-=2;
				else
					x+=2;
			if(lastUsed.equals("r"))
				if(!game.getColDet().checkCollision(x+2, y,this))
				x+=2;
				else
					x-=2;
		}
		else{
		}
		
		
	}
	
	public void lock()
	{
		locked=true;
	}
	
	public void unlock()
	{
		locked=false;
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		g.drawImage(Assets.sprites.get(0), (int) x - (int)game.getGameCamera().getxOffset(),
				(int) y - (int)game.getGameCamera().getyOffset(), 40,40, null);
		
	}
	
	
	private boolean isInTile(float coord)
	{
		if(coord%40 ==0)
		{
			return true;
		}
		return false;
		
	}
	public void setCoords(float x, float y)
	{
		this.x=x;
		this.y=y;
	}
	
	private BufferedImage returnImage(String direction)
	{
		switch(direction)
		{
		case "w": return Assets.sprites.get(193);
		
		case "d": return Assets.sprites.get(0);
		
		case "l": return Assets.sprites.get(192);
		
		case "r": return Assets.sprites.get(194);
		
		}
		return null;
	
	}

}
