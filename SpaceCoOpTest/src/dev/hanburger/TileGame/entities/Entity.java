package dev.hanburger.TileGame.entities;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;

import dev.hanburger.TileGame.Game;

public abstract class Entity {
	
	protected float x , y;
	protected Game game;
	
	protected Rectangle2D.Float box;
	
	public Entity(Game game,float x , float y)
	{
		this.x = x;
		this.y = y;
		this.game=game;
		box = new Rectangle2D.Float(x,y,40,40);
		game.getColDet().getEntities().add(this);
	}
	
	public void setBox(Rectangle2D.Float box) {
		this.box = box;
	}

	public Rectangle2D.Float getBox() {
		return box;
	}

	public abstract void tick();
	
	public abstract void render(Graphics g);

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

}

