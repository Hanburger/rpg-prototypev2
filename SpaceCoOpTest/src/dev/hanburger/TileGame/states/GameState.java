package dev.hanburger.TileGame.states;

import java.awt.Graphics;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.entities.creatures.Npc;
import dev.hanburger.TileGame.entities.creatures.Player;
import dev.hanburger.TileGame.gfx.DialogueManager;
import dev.hanburger.TileGame.gfx.MapManager;

public class GameState extends State {
	
	private Player player;
	
	private Npc npc;
	
	private MapManager mapManager;
	
	private DialogueManager dialogueMan;

	public GameState(Game game) {
		super(game);
		// TODO Auto-generated constructor stub
		player = new Player(game,520+120,520);
		npc = new Npc(game,520+160,560);
		mapManager= game.getMapManager();
		dialogueMan = new DialogueManager(game,player);
		
		
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		mapManager.tick();
		player.tick();
		npc.tick();
		dialogueMan.tick();
		
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		mapManager.render(g);
		player.render(g);
		npc.render(g);
		dialogueMan.render(g);
	}
	
	

}
