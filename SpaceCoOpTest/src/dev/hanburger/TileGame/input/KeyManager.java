package dev.hanburger.TileGame.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyManager implements KeyListener{

	private boolean[] keys;
	public boolean up, down, left, right, enter, x,c,xReleased=true;
	
	public KeyManager()
	{
		keys = new boolean[256];
	}
	
	
	public void tick()
	{
		
		if(x && !xReleased)
		{
			x=false;
		}
		
		up = keys[KeyEvent.VK_W];
		down = keys[KeyEvent.VK_S];
		left= keys[KeyEvent.VK_A];
		right = keys[KeyEvent.VK_D];
		enter = keys[KeyEvent.VK_ENTER];
		if(xReleased)
		x = keys[KeyEvent.VK_X];
		c = keys[KeyEvent.VK_C];
		
		
		if(x)
		{
			xReleased = false;
		}
		
		
	}
	@Override
	public void keyPressed(KeyEvent e) {
		keys[e.getKeyCode()] = true;
		//System.out.println("Pressed");
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		keys[e.getKeyCode()] = false;
		if(e.getKeyCode() == KeyEvent.VK_X)
		{
			xReleased=true;
		}
		
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
		
	}
	
	

}
